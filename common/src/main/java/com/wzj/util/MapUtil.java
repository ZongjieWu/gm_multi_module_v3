//package com.wzj.systemc.util;
//
//import com.spatial4j.core.context.SpatialContext;
//import com.spatial4j.core.shape.Rectangle;
//import io.vavr.Tuple4;
//
///**
// * MapUtil class
// * 位置的一些计算
// * @author ZongjieWu
// * @date 2020/09/09
// */
//public class MapUtil {
//
//    /**
//     * 获取距离指定经纬度的点{@code radius} KM 的外接四边形(严格来说应该是外接立方体)四个顶点的经纬度
//     *
//     * @param lng    经度
//     * @param lat    纬度
//     * @param radius 半径,单位:KM（要查看多少距离的餐柜）
//     * @return <lng1,lng2,lat1,lat2>
//     */
//    public static Tuple4<Double,Double,Double,Double> calcBoxByDistFromPt(double lng, double lat, double radius) {
//        SpatialContext context = SpatialContext.GEO;
//        Rectangle rectangle = context.getDistCalc()//
//                .calcBoxByDistFromPt(//
//                        context.makePoint(lng, lat), //
//                        radius * com.spatial4j.core.distance.DistanceUtils.KM_TO_DEG, context, null//
//                );
//        return new Tuple4(rectangle.getMinX(), rectangle.getMaxX(), rectangle.getMinY(), rectangle.getMaxY());
//    }
//
//
//    public static void main(String[] args) {
//        Tuple4 tuple4=MapUtil.calcBoxByDistFromPt(113.941108,22.544939,5);
//        System.out.println(tuple4._1);
//        System.out.println(tuple4._2);
//        System.out.println(tuple4._3);
//        System.out.println(tuple4._4);
//    }
//}
