package com.wzj.system_es.commodity.dto;

import lombok.Data;

@Data
public class PageCommodityRequest {

    /**
     * 当前页
     */
    private Integer currPage;

    /**
     * 每页大小
     */
    private Integer pageSize;
}
