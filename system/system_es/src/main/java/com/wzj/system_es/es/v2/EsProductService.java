package com.wzj.system_es.es.v2;

import com.wzj.object.result.Result;
import com.wzj.system_es.commodity.dto.DelProductDTO;
import com.wzj.system_es.commodity.dto.PageProductDTO;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EsProductService {

    @Autowired
    private EsProductRepository esProductRepository;

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;


    /**
     * 保存数据到es
     * @param esProduct
     */
    public void save(EsProduct esProduct) {
        esProductRepository.save(esProduct);
    }

    /**
     * 保存数据到es
     * @param esProductList es商品列表
     */
    public void saveAll(Iterable<EsProduct> esProductList) {
        esProductRepository.saveAll(esProductList);
    }


    /**
     * 从es删除数据
     * @param esProduct
     */
    public void delPro(EsProduct esProduct) {
        esProductRepository.delete(esProduct);
    }

    /**
     * 从es删除数据
     * @param esProduct
     */
    public void udpatePro(EsProduct esProduct) {
        esProductRepository.delete(esProduct);
        esProductRepository.save(esProduct);
    }

    /**
     * 保存数据到es
     */
    public List<EsProduct> findAll() {
        Iterable<EsProduct> esProductIterable =  esProductRepository.findAll();
        List<EsProduct> esProductList = new ArrayList<>();
        esProductIterable.forEach(obj->{esProductList.add(obj);});
        return esProductList;
    }

    /**
     * 简单搜索
     * @param pageProductDTO
     * @return
     */
    public Page<EsProduct> page(PageProductDTO pageProductDTO){
        QueryStringQueryBuilder builder = null;
        if(StringUtils.isNotBlank(pageProductDTO.getName())){
            builder = new QueryStringQueryBuilder(pageProductDTO.getName());;
        }
        Integer pageIndex = (pageProductDTO.getCurrPage()-1)*pageProductDTO.getPageSize();
        Pageable pageable = PageRequest.of(pageIndex,pageProductDTO.getPageSize());
        Page<EsProduct> pages = esProductRepository.search(builder,pageable);
        return  pages;
    }

    /**
     * 复杂搜索
     */
    public Page<EsProduct> mutilPage(PageProductDTO pageProductDTO){
        // 构建查询条件(搜索全部)
//        MatchAllQueryBuilder queryBuilder1 = QueryBuilders.matchAllQuery();
        // 搜索出 jobName 为 '主任' 的文档
//        TermQueryBuilder nameBuilder = QueryBuilders.termQuery("name", pageProductDTO.getName());
//        TermQueryBuilder keywordsBuilder = QueryBuilders.termQuery("keywords", pageProductDTO.getKeywords());
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        // must表示同时满足，should满足其中一个，must_not表示同时不满足
        if(StringUtils.isNotBlank(pageProductDTO.getName())){
            boolQueryBuilder.must(QueryBuilders.queryStringQuery(pageProductDTO.getName()));
        }
        if(StringUtils.isNotBlank(pageProductDTO.getKeywords())){
            boolQueryBuilder.must(QueryBuilders.matchQuery("keywords", pageProductDTO.getKeywords()));
        }
        // 分页
        Pageable pageable = PageRequest.of(pageProductDTO.getCurrPage() - 1, pageProductDTO.getPageSize());
        // 排序
        FieldSortBuilder balance = new FieldSortBuilder("id").order(SortOrder.DESC);
        // 执行查询
        NativeSearchQuery query = new NativeSearchQueryBuilder()
//                .withQuery(nameBuilder)
//                .withQuery(keywordsBuilder)
                .withQuery(boolQueryBuilder)
                .withPageable(pageable)
                .withSort(balance)
                .build();
        AggregatedPage<EsProduct> searchPage = elasticsearchRestTemplate.queryForPage(query, EsProduct.class);

        //封装page对象
        List<EsProduct> accounts = searchPage.getContent();
        Page<EsProduct> page = new PageImpl(accounts, pageable, searchPage.getTotalElements());

        return page;
    }


    public Result<List<String>> querySuggest(String keywords){
        // 使用suggest进行标题联想
        CompletionSuggestionBuilder suggest = SuggestBuilders.completionSuggestion("nameSuggest")
                //根据什么前缀来联想
                .prefix(keywords)
                // 跳过重复过滤
                .skipDuplicates(true)
                // 匹配数量
                .size(10);
        SuggestBuilder suggestBuilder = new SuggestBuilder();
        suggestBuilder.addSuggestion("name-suggest",suggest);

        //执行查询
        SearchResponse suggestResp = elasticsearchRestTemplate.suggest(suggestBuilder, EsProduct.class);

        //拿到Suggest结果
        Suggest.Suggestion<? extends Suggest.Suggestion.Entry<? extends Suggest.Suggestion.Entry.Option>> orderSuggest = suggestResp
                .getSuggest().getSuggestion("name-suggest");

        // 处理返回结果
        List<String> suggests = orderSuggest.getEntries().stream()
                .map(x -> x.getOptions().stream()
                        .map(y->y.getText().toString())
                        .collect(Collectors.toList())).findFirst().get();

        // 输出内容
//        for (String str : suggests) {
//            System.out.println("自动补全 = " + str);
//        }
        return Result.returnSucessMsgData(suggests);
    }
}
