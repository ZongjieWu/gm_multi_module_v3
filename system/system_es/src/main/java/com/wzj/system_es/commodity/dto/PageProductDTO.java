package com.wzj.system_es.commodity.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PageProductDTO extends PageCommodityRequest{
    private Long id;
    /**
     * 关键词
     */
    private String keywords;

    /**
     * 名称
     */
    private String name;

    /**
     * 价格
     */
    private BigDecimal price;
}
