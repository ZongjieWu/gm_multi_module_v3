package com.wzj.system_es.es.v2;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.completion.Completion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Document(indexName = "product_index_alias", type = EsProduct.TYPE, shards = 1, replicas = 0)
@Data
public class EsProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String TYPE = "_doc";

    @Id
    @Field(type = FieldType.Long)
    private Long id;

    //ik_max_word:会将文本做最细粒度的拆分，比如会将“中华人民共和国人民大会堂”拆分为“中华人民共和国、中华人民、中华、华人、人民共和国、人民、共和国、大会堂、大会、会堂等词语
    @Field(type = FieldType.Text)
    private String name;


//    @CompletionField(analyzer="ik_smart",searchAnalyzer="ik_smart", maxInputLength = 100)
//    private Completion nameSuggest;
    @CompletionField(analyzer="ik_smart",searchAnalyzer="ik_smart", maxInputLength = 100)
    private Completion nameSuggest;

    //ik_smart 会做最粗粒度的拆分，比如会将“中华人民共和国人民大会堂”拆分为中华人民共和国、人民大会堂。
    @Field(analyzer = "ik_smart", type = FieldType.Text)
    private String keywords;

    /**
     * 销售价
     */
    private BigDecimal price;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private Date createTime;

}
