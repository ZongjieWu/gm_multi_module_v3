package com.wzj.system_es.commodity.service.impl;

import com.wzj.object.result.Result;
import com.wzj.system_es.commodity.dto.DelProductDTO;
import com.wzj.system_es.commodity.dto.PageProductDTO;
import com.wzj.system_es.commodity.entity.Product;
import com.wzj.system_es.commodity.dao.ProductMapper;
import com.wzj.system_es.commodity.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.system_es.es.v2.EsProduct;
import com.wzj.system_es.es.v2.EsProductService;
import com.wzj.util.BeanCopyTools;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@AllArgsConstructor
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

    @Lazy
    private ProductMapper productMapper;

    @Lazy
    private EsProductService esProductService;

    /**
     * 添加商品，添加到数据库后再添加到es
     * @param product
     * @return
     */
    @Override
    public Result addPro(Product product) {
        productMapper.insertReturnId(product);
        //同步到es
        EsProduct esProduct = BeanCopyTools.copy(product,EsProduct.class);
        esProductService.save(esProduct);
        return Result.returnSucess();
    }

    @Override
    public Result delPro(DelProductDTO delProductDTO) {
        EsProduct esProduct = BeanCopyTools.copy(delProductDTO,EsProduct.class);
        esProductService.delPro(esProduct);
        return Result.returnSucess();
    }




    /**
     * 获取所有的商品,先从es获取，如果es没有在从数据库获取
     * @return
     */
    @Override
    public Result<List<EsProduct>> getAllPro() {
        List<EsProduct> esProductList = esProductService.findAll();
        if(!CollectionUtils.isEmpty(esProductList)){
            //从es获取
            //同步到e
            return Result.returnSucessMsgData(esProductList);
        }else{
            //从数据库获取数据
            List<Product> products = productMapper.selectList(null);
            List<EsProduct> esProducts = new ArrayList<>();
            BeanCopyTools.copy(products,esProducts,EsProduct.class);
            return Result.returnSucessMsgData(esProducts);
        }
    }

    /**
     * 分页获取所有的商品,先从es获取，如果es没有在从数据库获取
     * @param pageProductDTO
     * @return
     */
    @Override
    public Result<Page<EsProduct>> pagePro(PageProductDTO pageProductDTO) {
        Page<EsProduct> pagse =  esProductService.page(pageProductDTO);
        return Result.returnSucessMsgData(pagse);
//        if(false){
//            return null;
//        }else{
//            IPage<Product> userPage = new Page<>(pageProductDTO.getCurrPage(), pageProductDTO.getPageSize());//参数一是当前页，参数二是每页个数
//            userPage = productMapper.selectPage(userPage,null);
//            Result<List<Product>> result=Result.returnSucessMsgData(userPage.getRecords());
//            result.setCount(userPage.getTotal());
//            return result;
//        }
    }

    @Override
    public Result mutilPro(PageProductDTO pageProductDTO) {
        Page<EsProduct> pagse =  esProductService.mutilPage(pageProductDTO);
        return Result.returnSucessMsgData(pagse);
    }

    @Override
    public Result syncDBtoEs() {
        //查询数据库所有的数据
        List<Product> dbProductList = productMapper.selectList(null);
        if(CollectionUtils.isEmpty(dbProductList)){
           return Result.returnFailMsg("暂无数据不用同步");
        }


        //数据库转变es
        List<EsProduct> esProducts = new ArrayList<>();
        EsProduct esProduct = null;
//        BeanCopyTools.copy(dbProductList,esProducts,EsProduct.class);
        for(Product product : dbProductList){
            esProduct = new EsProduct();
            esProduct.setId(product.getId());
            esProduct.setKeywords(product.getKeywords());
            esProduct.setPrice(product.getPrice());
            esProduct.setSort(product.getSort());
            esProduct.setName(product.getName());
            esProduct.setCreateTime(product.getCreateTime());
            List<String> nameList =  new ArrayList<>();
            nameList.add(product.getName()); //可以把多个内容作为suggest的数据源
            Completion suggestCompletion = new Completion(nameList.toArray(new String[nameList.size()]));
            esProduct.setNameSuggest(suggestCompletion);
            esProducts.add(esProduct);
        }
        esProductService.saveAll(esProducts);
        return Result.returnSucessMsg("同步所有商品数据到ES成功");
    }

    @Override
    public Result<List<String>> querySuggest(String keywords) {
        return esProductService.querySuggest(keywords);
    }
}
