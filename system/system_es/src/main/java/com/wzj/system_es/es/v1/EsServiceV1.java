//package com.wzj.system_es.es.v1;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.TypeReference;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
//import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
//import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
//import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
//import org.elasticsearch.action.delete.DeleteRequest;
//import org.elasticsearch.action.delete.DeleteResponse;
//import org.elasticsearch.action.index.IndexRequest;
//import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.action.support.master.AcknowledgedResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.unit.TimeValue;
//import org.elasticsearch.common.xcontent.XContentBuilder;
//import org.elasticsearch.common.xcontent.XContentFactory;
//import org.elasticsearch.common.xcontent.XContentType;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.FuzzyQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.index.query.TermQueryBuilder;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.TimeUnit;
//
//@Slf4j
//@Service
//public class EsServiceV1 {
//    @Autowired
//    private RestHighLevelClient restHighLevelClient;
//
//    @Autowired
//    private ElasticsearchRestTemplate elasticsearchRestTemplate;
//
//
//    /**
//     * 创建索引
//     * @param indexNam 索引名称
//     * @return
//     */
//    public Boolean createIndex(String indexNam) {
//        CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexNam.toLowerCase());
//        createIndexRequest.settings(Settings.builder().put("index.number_of_shards", 4)
//                .put("index.number_of_replicas", 2));
//        try {
//            XContentBuilder xContentBuilder= XContentFactory.jsonBuilder();
//            xContentBuilder.startObject().startObject("properties")
//                    .startObject("type").field("type", "integer").endObject()
//                    .startObject("content").field("type", "text").field("analyzer", "ik_max_word").endObject()
//                    .endObject().endObject();
//            createIndexRequest.mapping(xContentBuilder.toString());
//            CreateIndexResponse response = restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
//            boolean acknowledged = response.isAcknowledged();
//            // 指示是否在超时之前为索引中的每个分片启动了必需的分片副本数
//            boolean shardsAcknowledged = response.isShardsAcknowledged();
//            if (acknowledged || shardsAcknowledged) {
//                log.info("创建索引成功！索引名称为{}", indexNam);
//                return Boolean.TRUE;
//            }
//        } catch (IOException e) {
//            log.info("创建索引失败{}",e);
//        }
//        return Boolean.FALSE;
//    }
//
//    public Boolean isIndexExists(final String indexName) {
//        try {
//            GetIndexRequest request = new GetIndexRequest();
//            request.indices(indexName);
//            return restHighLevelClient.indices().exists(request,RequestOptions.DEFAULT);
//        } catch (IOException e) {
//            log.info("查看索引是否存在失败{}",e);
//            return Boolean.FALSE;
//        }
//    }
//
//    /**
//     * 移除索引
//     * @param indexName 索引名称
//     * @return
//     */
//    public Boolean removeIndex(String indexName) {
//        try {
//            DeleteIndexRequest deleteIndexRequest=new DeleteIndexRequest(indexName.toLowerCase());
//            AcknowledgedResponse acknowledgedResponse=restHighLevelClient.indices().delete(deleteIndexRequest,RequestOptions.DEFAULT);
//            if(acknowledgedResponse.isAcknowledged()){
//                return Boolean.TRUE;
//            }
//        } catch (IOException e) {
//            log.error("删除索引失败{}",e);
//        }
//        return Boolean.FALSE;
//    }
//
//    public Boolean saveDocment(ElasticSearchCommon elasticSearchCommon) {
//        if(!this.isIndexExists(elasticSearchCommon.getIndexName().toLowerCase())){
//            this.createIndex(elasticSearchCommon.getIndexName());
//        }
//        IndexRequest indexRequest=new IndexRequest(elasticSearchCommon.getIndexName());
//        indexRequest.id(elasticSearchCommon.getId());
//        indexRequest.timeout("3s");
//        indexRequest.source(JSON.toJSONString(elasticSearchCommon), XContentType.JSON);
//        IndexResponse response = null;
//        try {
//            response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
//        } catch (IOException e) {
//            log.info("存储文档失败{}",e);
//        }
//        return response.getShardInfo().getSuccessful()>0?true:false;
//    }
//
//    public Boolean delDocment(String id, String indexName) {
//        DeleteRequest deleteRequest=new DeleteRequest();
//        deleteRequest.timeout("3s");
//        deleteRequest.id(id);
//        DeleteResponse deleteResponse = null;
//        try {
//            deleteResponse = restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
//        } catch (IOException e) {
//            log.info("删除文档失败{}",e);
//        }
//        return deleteResponse.getShardInfo().getSuccessful()>0?true:false;
//    }
//
//    public <E> PageResult<E> searchIndex(final ElasticSearchRequestBean elasticSearchRequestBean,Class<E> ep) {
//        SearchRequest searchRequest = new SearchRequest(elasticSearchRequestBean.getIndexName());
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.from((elasticSearchRequestBean.getPage().getPageNum()-1)*elasticSearchRequestBean.getPage().getPageSize());
//        searchSourceBuilder.size(elasticSearchRequestBean.getPage().getPageSize());
//        searchSourceBuilder.trackTotalHits(true);//用于显示正确的总数
//        BoolQueryBuilder boolQueryBuilder= QueryBuilders.boolQuery();
//        if(StringUtils.isNotEmpty(elasticSearchRequestBean.getKeyWord())){
//            FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("content", elasticSearchRequestBean.getKeyWord());
//            boolQueryBuilder.must(fuzzyQueryBuilder);
//        }
//        if(elasticSearchRequestBean.getType()!=null){
//            TermQueryBuilder termQueryBuilder=QueryBuilders.termQuery("type",elasticSearchRequestBean.getType());
//            boolQueryBuilder.must(termQueryBuilder);
//        }
//        searchSourceBuilder.query(boolQueryBuilder);
//        searchSourceBuilder.timeout(new TimeValue(3, TimeUnit.SECONDS));
//        searchRequest.source(searchSourceBuilder);
//        PageResult<E> pageResult=new PageResult<>();
//        List<E> list=new ArrayList();
//        Long total=0L;
//        try {
//            SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
//            if (search.getHits().getHits().length!=0){
//                Map<Object,Object> map=null;
//                for (SearchHit documentFields : search.getHits().getHits()) {
//                    map=new HashMap<>();
//                    map.putAll(documentFields.getSourceAsMap());
//                    list.add(JSONObject.parseObject(JSONObject.toJSONString(map), new TypeReference<E>(){}));
//                }
//                total=search.getHits().getTotalHits();
//            }
//            pageResult.setRecord(list);
//            pageResult.setTotalCount(total);
//            pageResult.setCurrentPage(elasticSearchRequestBean.getPage().getPageNum());
//            pageResult.setPageSize(elasticSearchRequestBean.getPage().getPageSize());
//            pageResult.setPageCount(total/elasticSearchRequestBean.getPage().getPageSize());
//            return pageResult;
//        } catch (IOException e) {
//            log.info("搜索失败{}",e);
//        }
//        return pageResult;
//    }
//
//
//
//}
