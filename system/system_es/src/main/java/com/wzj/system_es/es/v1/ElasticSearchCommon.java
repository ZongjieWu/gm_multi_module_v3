package com.wzj.system_es.es.v1;

import lombok.Data;

import java.util.UUID;

@Data
public class ElasticSearchCommon {
    private String id = UUID.randomUUID().toString().replace("-","");
    private String indexName;
}
