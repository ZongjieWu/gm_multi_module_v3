package com.wzj.system_es.commodity.dao;

import com.wzj.object.result.Result;
import com.wzj.system_es.commodity.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wzj.system_es.es.v2.EsProduct;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Repository
public interface ProductMapper extends BaseMapper<Product> {
    /**
     * 插入并返回ID
     * @param product
     * @return
     */
    int insertReturnId(Product product);
}
