package com.wzj.system_es.commodity.dto;

import lombok.Data;

@Data
public class DelProductDTO {
    /**
     * 商品ID
     */
    private Long id;
}
