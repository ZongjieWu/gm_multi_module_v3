package com.wzj.system_es.es.v1;

import lombok.Data;

@Data
public class ElasticSearchRequestBean {
    private String indexName;
    private String KeyWord;
    private Integer type;
    private Page page;
}
