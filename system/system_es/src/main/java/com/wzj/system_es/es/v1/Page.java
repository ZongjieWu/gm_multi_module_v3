package com.wzj.system_es.es.v1;

import lombok.Data;

@Data
public class Page {
    private Integer pageNum;
    private Integer pageSize;
}
