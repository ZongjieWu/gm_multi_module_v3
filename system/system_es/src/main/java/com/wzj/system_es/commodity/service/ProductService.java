package com.wzj.system_es.commodity.service;

import com.wzj.object.result.Result;
import com.wzj.system_es.commodity.dto.DelProductDTO;
import com.wzj.system_es.commodity.dto.PageProductDTO;
import com.wzj.system_es.commodity.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.system_es.es.v2.EsProduct;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
public interface ProductService extends IService<Product> {
    /**
     * 添加商品，添加到数据库后再添加到es
     * @param product
     * @return
     */
    Result addPro(Product product);

    /**
     * 删除商品，删除数据库后再删除到es
     * @param delProductDTO
     * @return
     */
    Result delPro(DelProductDTO delProductDTO);

    /**
     * 获取所有的商品,先从es获取，如果es没有在从数据库获取
     * @return
     */
    Result getAllPro();

    /**
     * 分页获取所有的商品,先从es获取，如果es没有在从数据库获取
     * @param pageProductDTO
     * @return
     */
    Result pagePro(PageProductDTO pageProductDTO);

    /**
     * 多个分页查询
     * @param pageProductDTO
     * @return
     */
    Result mutilPro(PageProductDTO pageProductDTO);

    /**
     * 同步数据库tb_product所有数据到Es
     * @return
     */
    Result syncDBtoEs();

    Result<List<String>> querySuggest(String keywords);
}
