package com.wzj.system_es.es.v1;

import lombok.Data;

import java.util.List;

@Data
public class PageResult<T> {
    private List<T> record;
    private Long totalCount;
    private Integer currentPage;
    private Integer pageSize;
    private Long pageCount;
}
