package com.wzj.system_es.commodity.controller;


import com.wzj.object.result.Result;
import com.wzj.system_es.commodity.dto.DelProductDTO;
import com.wzj.system_es.commodity.dto.PageProductDTO;
import com.wzj.system_es.commodity.entity.Product;
import com.wzj.system_es.commodity.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 *  增删改查，分页例子
 * @author ZongjieWu
 * @since 2021-03-31
 */
@RestController
@RequestMapping("/product")
public class ESCommodityController {

    @Autowired
    private ProductService productService;

    /**
     * 添加商品，添加到数据库后再添加到es
     * @param product
     * @return
     */
    @PostMapping("addPro")
    public Result addPro(@RequestBody Product product){
        return productService.addPro(product);
    }

    /**
     * 删除商品，添加到数据库后再添加到es
     * @param delProductDTO
     * @return
     */
    @PostMapping("delPro")
    public Result addPro(@RequestBody DelProductDTO delProductDTO){
        return productService.delPro(delProductDTO);
    }

    /**
     * 获取所有商品
     * @return
     */
    @GetMapping("getAllPro")
    public Result getAllPro(){
        return productService.getAllPro();
    }

    /**
     * 分页商品
     * @param pageProductDTO
     * @return
     */
    @PostMapping("pagePro")
    public Result pagePro(@RequestBody PageProductDTO pageProductDTO){
        return productService.pagePro(pageProductDTO);
    }

    /**
     * 查询建议字符
     * @return
     */
    @GetMapping("suggest")
    public Result suggest(String keywords){
        return productService.querySuggest(keywords);
    }

    /**
     * 复杂分页商品查询
     * @param pageProductDTO
     * @return
     */
    @PostMapping("mutilPro")
    public Result mutilPro(@RequestBody PageProductDTO pageProductDTO){
        return productService.mutilPro(pageProductDTO);
    }


    /**
     * 同步数据库数据到Es
     * @return
     */
    @PostMapping("syncDBtoEs")
    public Result syncDBtoEs(){
        return productService.syncDBtoEs();
    }
}

