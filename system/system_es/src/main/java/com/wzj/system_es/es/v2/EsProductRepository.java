package com.wzj.system_es.es.v2;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface EsProductRepository extends ElasticsearchRepository<EsProduct, Long> {


}
