package com.wzj.systema.controller;

import com.wzj.object.result.Result;
import com.wzj.systema.entity.User;
import com.wzj.systema.service.UserService;
import com.wzj.systema.service.impl.SpringAsync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zongjie Wu
 * @date 2021/3/22 15:39
 */
@RequestMapping("test")
@RestController
public class TestController {

    @Autowired
    private UserService userService;

    @Autowired
    private SpringAsync springAsync;
    /**
     * 测试程序提交  分支到分支1
     * @return
     */
    @GetMapping(value = "test")
    public Result<User> test(){
        User user=userService.getById(1L);
        return Result.returnSucessMsgData(user);
//        springAsync.async();
    }
}
