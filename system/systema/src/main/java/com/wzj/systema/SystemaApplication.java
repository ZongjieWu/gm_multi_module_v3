package com.wzj.systema;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Repository;

@EnableAsync
@EnableRetry //开启重试机制
@MapperScan(basePackages = {"com.wzj"}, annotationClass = Repository.class)
@ComponentScan(basePackages={"com.wzj"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SystemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemaApplication.class, args);
	}

}
