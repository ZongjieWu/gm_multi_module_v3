package com.wzj.systema.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SpringRetryDemo   {

    /**
     * 重试所调用方法
     * @param param
     * @return
     */
    @Retryable(value = {RemoteAccessException.class},maxAttempts = 5 ,backoff = @Backoff(delay = 1000L,multiplier = 2))
    public boolean call(String param){
        return retryTask(param);
    }

    /**
     * 达到最大重试次数,或抛出了一个没有指定进行重试的异常
     * recover 机制
     * @param e 异常
     */
    @Recover
    public boolean recover(Exception e,String param) {
        log.error("达到最大重试次数,或抛出了一个没有指定进行重试的异常:",e);
        return false;
    }



    /**
     * 重试方法
     * @return
     */
    public static boolean retryTask(String param)  {
        log.info("收到请求参数:{}",param);
        log.info("当前线程的名字："+Thread.currentThread().getName());
        int i = RandomUtils.nextInt(0,20);
        log.info("随机生成的数:{}",i);
        if (i == 0) {
            log.info("为0,抛出参数异常.");
            throw new IllegalArgumentException("参数异常");
        }else if (i  == 1){
            log.info("为1,返回true.");
            return true;
        }else if (i == 2){
            log.info("为2,返回false.");
            return false;
        }else{
            //为其他
            log.info("大于2,抛出自定义异常.");
            throw new RemoteAccessException("大于2,抛出远程访问异常");
        }
    }

}