package com.wzj.systema.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SpringAsync {

    @Autowired
    private SpringRetryDemo springRetryDemo;

    @Async
    public void async(){
        boolean abc = springRetryDemo.call("abc");
        log.info("--结果是:{}--",abc);

    }
}
