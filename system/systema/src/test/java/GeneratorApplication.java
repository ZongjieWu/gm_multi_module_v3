import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GeneratorApplication {


    public static void main(String[] args) throws Exception {
        System.out.println("根据数据库生成文件-开始");
        List<String> warnings = new ArrayList<String>();
        //是否覆盖原文件
        boolean overwrite = true;
        String location = new GeneratorApplication().getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        File configFile = new File(location + "mybatis-generator.xml");//直接放在文件下面，与pom.xml同级
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        System.out.println("根据数据库生成文件-结束");
    }
}
