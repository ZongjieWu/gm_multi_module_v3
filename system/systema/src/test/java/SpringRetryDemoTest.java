import com.wzj.systema.service.impl.SpringAsync;
import com.wzj.systema.service.impl.SpringRetryDemo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SpringRetryDemoTest extends RetryTets {

    @Autowired
    private SpringAsync springAsync;

    @Test
    public void retry(){
        springAsync.async();
    }

}
