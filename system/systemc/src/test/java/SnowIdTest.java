
import com.wzj.systemc.SystemcApplication;
import com.wzj.systemc.user.dao.SnowflakeIdWorker;
import com.wzj.systemc.user.dao.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SystemcApplication.class)// 指定启动类
public class SnowIdTest {
    @Resource
    private UserMapper userMapper;
    @Resource
    private SnowflakeIdWorker idWorker;

    @Test
    public void testSnowId(){
        System.out.println(userMapper.getSnowId(1L));
    }

    @Test
    public void insertSonwid(){
        for(int i=0,count=100000000;i<count;i++){
            userMapper.insertSonwId(idWorker.nextId());
        }
    }

}
