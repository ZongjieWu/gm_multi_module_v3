package com.wzj.systemc.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.object.result.Result;
import com.wzj.systemc.order.dto.OrderMasterDataDTO;
import com.wzj.systemc.order.entity.OrderMaster;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
public interface IOrderMasterService extends IService<OrderMaster> {
    /**
     * 根据id查询详情
     * @param userId
     * @return
     */
    Result<OrderMasterDataDTO> detailById(Long userId);
}
