package com.wzj.systemc.permission.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.role.SaasUserRoleTypeAddRequestVo;
import com.wzj.systemc.permission.dto.role.SaasUserRoleTypePagingRequestVo;
import com.wzj.systemc.permission.dto.role.SaasUserRoleTypeUpdateRequestVo;
import com.wzj.systemc.permission.entity.SaasUserRolePermission;
import com.wzj.systemc.permission.entity.SaasUserRoleType;
import com.wzj.systemc.permission.service.ISaasUserRolePermissionService;
import com.wzj.systemc.permission.service.ISaasUserRoleTypeService;
import com.wzj.util.BeanCopyTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 *  3.平台管理员角色,添加、修改、删除、查询等接口
 * @author ZongjieWu
 * @since 2020-04-20
 */

@RestController
@RequestMapping("/saasUserRoleType/")
public class SaasUserRoleTypeController {

    @Autowired
    private ISaasUserRoleTypeService saasUserRoleTypeService;
    @Autowired
    private ISaasUserRolePermissionService saasUserRolePermissionService;

    /**
     * 平台管理员角色添加
     * @param saasUserRoleTypeAddRequestVo
     * @return
     */
//    @SocketRole(value = "add_saasUserRoleType")
    @PostMapping("add")
    public Result add(@Validated SaasUserRoleTypeAddRequestVo saasUserRoleTypeAddRequestVo) {
        SaasUserRoleType saasUserRoleType= BeanCopyTools.copy(saasUserRoleTypeAddRequestVo,SaasUserRoleType.class);
        saasUserRoleType.setAddTime(new Date());
        saasUserRoleType.setModifyTime(new Date());
        saasUserRoleTypeService.save(saasUserRoleType);
        return Result.returnSucess();
    }

    /**
     * 平台管理员角色的删除
     * @param id
     * @return
     */
//    @SocketRole(value = "delete_saasUserRoleType")
    @PostMapping("delete")
    public Result delete(@RequestParam Long id) {
        saasUserRoleTypeService.removeById(id);
        return Result.returnSucess();
    }

    /**
     * 平台管理员角色修改
     * @param saasUserRoleTypeUpdateRequestVo
     * @return
     */
//    @SocketRole(value = "update_saasUserRoleType")
    @PostMapping("update")
    public Result update(@Validated SaasUserRoleTypeUpdateRequestVo saasUserRoleTypeUpdateRequestVo) {
        SaasUserRoleType saasUserRoleType= BeanCopyTools.copy(saasUserRoleTypeUpdateRequestVo,SaasUserRoleType.class);
        saasUserRoleType.setModifyTime(new Date());
        saasUserRoleTypeService.updateById(saasUserRoleType);
        return Result.returnSucess();
    }

//    @ApiIgnore
//    @SocketRole(value = "detail_saasUserRoleType")
//    @GetMapping("detail")
//    public Result<SaasUserRoleType> detail(@RequestParam Long id) {
//        SaasUserRoleType saasUserRoleType = saasUserRoleTypeService.findById(id);
//        return Result.retrunSucessMsgData(saasUserRoleType);
//    }

    /**
     * 获取角色列表
     * @return
     */
//    @SocketRole(value = "list_saasUserRoleType")
    @GetMapping("list")
    public Result<List<SaasUserRoleType>> list() {
        List<SaasUserRoleType> saasUserRoleType = saasUserRoleTypeService.list();
        return Result.returnSucessMsgData(saasUserRoleType);
    }

//    @SocketRole(value = "pageing_saasUserRoleType")
    @PostMapping("paging")
    public Result<List<SaasUserRoleType>> paging(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer limit, @Validated SaasUserRoleTypePagingRequestVo saasUserRoleTypePagingRequestVo) {
        IPage<SaasUserRoleType> saasUserRoleTypeIPage = new Page<>(page, limit);//参数一是当前页，参数二是每页个数
        //组装查询器
        QueryWrapper<SaasUserRoleType> saasUserQueryWrapper=new QueryWrapper<SaasUserRoleType>();
        saasUserQueryWrapper.lambda()
        .like(!StringUtils.isEmpty(saasUserRoleTypePagingRequestVo.getKeywords()),SaasUserRoleType::getName,saasUserRoleTypePagingRequestVo.getKeywords());
        IPage<SaasUserRoleType> saasUserRoleTypeIPage1= saasUserRoleTypeService.page(saasUserRoleTypeIPage,saasUserQueryWrapper);
        List<SaasUserRoleType> saasUserRoleTypeList=saasUserRoleTypeIPage1.getRecords();
        Result<List<SaasUserRoleType>> result=Result.returnSucessMsgData(saasUserRoleTypeList);
        result.setCount(saasUserRoleTypeIPage1.getTotal());
        return result;
    }

    /**
     * 平台管理员角色权限的修改
     * @param rid 角色id
     * @param authids 权限id字符串,用逗号(,)隔开
     * @return
     */
    @PostMapping("updatePermission")
    public Result updatePermission(Long rid, String authids){
        String[] authidsArr=authids.split(",");
        /*
        * 先删除再添加
        * **/
        SaasUserRolePermission erp=new SaasUserRolePermission();
        erp.setSaasUserRoleId(rid);
        try {
            synchronized (SaasUserRolePermission.class){
                QueryWrapper<SaasUserRolePermission> saasUserRolePermissionQueryWrapper=new QueryWrapper<>();
                saasUserRolePermissionQueryWrapper.lambda().eq(SaasUserRolePermission::getSaasUserRoleId,rid);
                saasUserRolePermissionService.remove(saasUserRolePermissionQueryWrapper);
            }
            if(authids.trim().length()>0){
                SaasUserRolePermission saasUserRolePermission=null;
                for(int i=0;i<authidsArr.length;i++){
                    saasUserRolePermission=new SaasUserRolePermission();
                    saasUserRolePermission.setSaasUserRoleId(rid);
                    saasUserRolePermission.setSaasUserPermissionId(Long.parseLong(authidsArr[i]));
                    saasUserRolePermissionService.save(saasUserRolePermission);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return Result.returnSucess();
    }

}


