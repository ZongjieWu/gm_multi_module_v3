package com.wzj.systemc.permission.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserPagingRequestVo;
import com.wzj.systemc.permission.entity.SaasUser;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-19
 */
public interface ISaasUserService extends IService<SaasUser> {
    /**
     * 分页查询
     * @param page
     * @param limit
     * @param saasUserPagingRequestVo
     * @return
     */
    Result<List<SaasUserBaseInfoResponseVo>> paging(Integer page, Integer limit, SaasUserPagingRequestVo saasUserPagingRequestVo);

    /**
     * 根据token查询用户数据
     * @param token
     * @return
     */
    Result<SaasUserBaseInfoResponseVo> getByToken(String token);
}
