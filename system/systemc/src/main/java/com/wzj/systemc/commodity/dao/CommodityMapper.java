package com.wzj.systemc.commodity.dao;

import com.wzj.systemc.commodity.entity.Commodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Repository
public interface CommodityMapper extends BaseMapper<Commodity> {

}
