package com.wzj.systemc.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.object.result.Result;
import com.wzj.systemc.order.dao.OrderMasterMapper;
import com.wzj.systemc.order.dto.OrderMasterDataDTO;
import com.wzj.systemc.order.entity.OrderMaster;
import com.wzj.systemc.order.service.IOrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Service
public class OrderMasterServiceImpl extends ServiceImpl<OrderMasterMapper, OrderMaster> implements IOrderMasterService {

    @Autowired
    private OrderMasterMapper orderMasterMapper;

    @Override
    public Result<OrderMasterDataDTO> detailById(Long userId) {
        OrderMasterDataDTO orderMasterDataDTO = orderMasterMapper.detailById(userId);
        return Result.returnSucessMsgData(orderMasterDataDTO);
    }
}
