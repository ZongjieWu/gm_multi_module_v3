package com.wzj.systemc.permission.dto.permission;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 获取权限列表请求数据
 */
@Data
public class SaasUserPermissionPagingRequestVo {
    /**
     * 父级权限id
     */
    @NotNull
    private Long parentId;

    /**
     * 搜索关键词
     */
    private String keywords;
}
