package com.wzj.systemc.permission.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserLoginRequestVo;
import com.wzj.systemc.permission.entity.SaasUser;

public interface ISaasUserLoginService  extends IService<SaasUser> {
    Result<SaasUserBaseInfoResponseVo> login(SaasUserLoginRequestVo saasUserLoginRequestVo);
}
