package com.wzj.systemc.user.dto;

import lombok.Data;

/**
 * 顾客登入
 */
@Data
public class UserLoginRequestDTO {
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户密码
     */
    private String pwd;
}
