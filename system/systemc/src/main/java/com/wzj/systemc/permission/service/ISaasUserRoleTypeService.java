package com.wzj.systemc.permission.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.systemc.permission.entity.SaasUserRoleType;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
public interface ISaasUserRoleTypeService extends IService<SaasUserRoleType> {

}
