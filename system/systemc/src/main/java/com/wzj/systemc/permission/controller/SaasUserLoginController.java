package com.wzj.systemc.permission.controller;

import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserLoginRequestVo;
import com.wzj.systemc.permission.service.ISaasUserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 1.平台管理员
 */
@RestController
@RequestMapping("/admin")
public class SaasUserLoginController {
    @Autowired
    private ISaasUserLoginService saasUserLoginService;

    /**
     * 登入
     * @param loginRequestVo
     * @return
     */
    @PostMapping("/login")
    public Result<SaasUserBaseInfoResponseVo> add( @Validated SaasUserLoginRequestVo loginRequestVo) {
        return saasUserLoginService.login(loginRequestVo);
    }
}
