package com.wzj.systemc.permission.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.systemc.permission.entity.SaasUserRolePermission;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
public interface ISaasUserRolePermissionService extends IService<SaasUserRolePermission> {
    List<Long> listByRoleId(Long saasUserRoleId);
}
