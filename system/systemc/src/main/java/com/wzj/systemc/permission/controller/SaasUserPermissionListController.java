package com.wzj.systemc.permission.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.permission.SaasUserPermissionAddRequestVo;
import com.wzj.systemc.permission.dto.permission.SaasUserPermissionPagingRequestVo;
import com.wzj.systemc.permission.dto.permission.SaasUserPermissionUpdateRequestVo;
import com.wzj.systemc.permission.entity.SaasUserPermissionList;
import com.wzj.systemc.permission.service.ISaasUserPermissionListService;
import com.wzj.systemc.permission.service.ISaasUserRolePermissionService;
import com.wzj.util.BeanCopyTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  5.平台管理员权限,添加、修改、删除、查询等接口
 * @author ZongjieWu
 * @since 2020-04-20
 */
@RestController
@RequestMapping("/saasUserPermissionList/")
public class SaasUserPermissionListController {

    @Autowired
    private ISaasUserPermissionListService saasUserPermissionListService;
    @Autowired
    private ISaasUserRolePermissionService saasUserRolePermissionService;

    /**
     * 添加权限
     * @param saasUserPermissionAddRequestVo
     * @return
     */
//    @SocketRole(value = "add_saasUserPermission")
    @PostMapping("add")
    public Result add(@Validated SaasUserPermissionAddRequestVo saasUserPermissionAddRequestVo) {
        SaasUserPermissionList saasUserPermissionList= BeanCopyTools.copy(saasUserPermissionAddRequestVo,SaasUserPermissionList.class);
        saasUserPermissionList.setAddTime(new Date());
        saasUserPermissionList.setModifyTime(new Date());
        saasUserPermissionListService.save(saasUserPermissionList);
        return Result.returnSucess();
    }

    /**
     * 删除权限
     * @param id
     * @return
     */
//    @SocketRole(value = "delete_saasUserPermission")
    @PostMapping("delete")
    public Result delete(@RequestParam Long id) {
        saasUserPermissionListService.removeById(id);
        return Result.returnSucess();
    }

    /**
     * 修改权限
     * @param saasUserPermissionUpdateRequestVo
     * @return
     */
//    @SocketRole(value = "update_saasUserPermission")
    @PostMapping("update")
    public Result update(@Validated SaasUserPermissionUpdateRequestVo saasUserPermissionUpdateRequestVo) {
        SaasUserPermissionList saasUserPermissionList= BeanCopyTools.copy(saasUserPermissionUpdateRequestVo,SaasUserPermissionList.class);
        saasUserPermissionList.setModifyTime(new Date());
        saasUserPermissionListService.updateById(saasUserPermissionList);
        return Result.returnSucess();
    }

//    @SocketRole(value = "detail_saasUserPermission")
    @GetMapping("detail")
    public Result<SaasUserPermissionList> detail(@RequestParam Long id) {
        SaasUserPermissionList saasUserPermissionList = saasUserPermissionListService.getById(id);
        return Result.returnSucessMsgData(saasUserPermissionList);
    }

    /***
     * 分页查询管理员权限
     * @param page
     * @param limit
     * @param saasUserPermissionPagingRequestVo
     * @return
     */
    //    @SocketRole(value = "pageing_saasUserPermission")
    @PostMapping("paging")
    public Result<List<SaasUserPermissionList>> paging(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer limit, @Validated SaasUserPermissionPagingRequestVo saasUserPermissionPagingRequestVo) {
        IPage<SaasUserPermissionList> saasUserRoleTypeIPage = new Page<>(page, limit);//参数一是当前页，参数二是每页个数
        //组装查询器
        QueryWrapper<SaasUserPermissionList> saasUserQueryWrapper=new QueryWrapper<SaasUserPermissionList>();
        saasUserQueryWrapper.lambda()
                .eq(SaasUserPermissionList::getParentId,saasUserPermissionPagingRequestVo.getParentId())
                .like(!StringUtils.isEmpty(saasUserPermissionPagingRequestVo.getKeywords()),SaasUserPermissionList::getName,saasUserPermissionPagingRequestVo.getKeywords());
        saasUserQueryWrapper.orderByAsc("sort");
        IPage<SaasUserPermissionList> saasUserRoleTypeIPage1= saasUserPermissionListService.page(saasUserRoleTypeIPage,saasUserQueryWrapper);
        List<SaasUserPermissionList> saasUserRoleTypeList=saasUserRoleTypeIPage1.getRecords();
        Result<List<SaasUserPermissionList>> result=Result.returnSucessMsgData(saasUserRoleTypeList);
        result.setCount(saasUserRoleTypeIPage1.getTotal());
        return result;
    }

    @PostMapping("listByRoleId")
    public Map<String,Object> listByRoleId(Long saasUserRoleId){
        List<SaasUserPermissionList> permissionList=saasUserPermissionListService.list();
        Map<String,Object> listByRoleIdMap=new HashMap<>();
        listByRoleIdMap.put("roleId",saasUserRoleId);
        List<Long> permissionIdList=saasUserRolePermissionService.listByRoleId(saasUserRoleId);


        Map<String,Object> map=new HashMap<>();
        Map<String,Object> m=new HashMap<>();


        m.put("list",permissionList);
        m.put("checkedId",permissionIdList.toArray());
        m.put("roleId",saasUserRoleId);
        map.put("code",0);
        map.put("msg","获取成功");
        map.put("data",m);
        return map;
    }
}


