package com.wzj.systemc.user.service;

import com.wzj.object.result.Result;
import com.wzj.systemc.user.dto.UserLoginRequestDTO;
import com.wzj.systemc.user.dto.UserLoginResponseDTO;
import com.wzj.systemc.user.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
public interface IUserService extends IService<User> {
    /**
     * 用户登入
     * @param requestDTO
     * @return
     */
    Result<UserLoginResponseDTO> userLogin(UserLoginRequestDTO requestDTO);

    Long getSnowId(Long snowId);
}
