package com.wzj.systemc.user.service.impl;

import com.wzj.object.result.Result;
import com.wzj.systemc.user.dto.UserLoginRequestDTO;
import com.wzj.systemc.user.dto.UserLoginResponseDTO;
import com.wzj.systemc.user.entity.User;
import com.wzj.systemc.user.dao.UserMapper;
import com.wzj.systemc.user.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public Result<UserLoginResponseDTO> userLogin(UserLoginRequestDTO requestDTO) {
        return null;
    }

    @Override
    public Long getSnowId(Long snowId) {
        Long id=userMapper.getSnowId(snowId);
        System.out.println(id);
        return id;
    }
}
