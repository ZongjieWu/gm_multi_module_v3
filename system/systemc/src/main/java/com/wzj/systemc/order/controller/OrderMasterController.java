package com.wzj.systemc.order.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wzj.object.result.Result;
import com.wzj.systemc.order.dto.OrderMasterDataDTO;
import com.wzj.systemc.order.entity.OrderMaster;
import com.wzj.systemc.order.service.IOrderMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 订单相关接口
 * @author ZongjieWu
 * @since 2021-03-23
 */
@RestController
@RequestMapping("/order")
public class OrderMasterController {

    @Autowired
    private IOrderMasterService orderService;

    /**
     * 根据用户id获取订单
     * @param userId 用户id
     * @return
     */
    @GetMapping(value = "getByUserId")
    public Result<List<OrderMaster>> getByUserId(Long userId){
        QueryWrapper<OrderMaster> queryWrapper = new QueryWrapper<OrderMaster>();
        queryWrapper.lambda().eq(OrderMaster::getUserId, userId);
        List<OrderMaster> orderMasterList =orderService.list(queryWrapper);
        return Result.returnSucessMsgData(orderMasterList);
    }


    /**
     * 根据id获取订单详情
     * @param id 订单id
     * @return
     */
    @GetMapping(value = "detailById")
    public Result<OrderMasterDataDTO> detailById(Long id){
        return orderService.detailById(id);
    }

}

