package com.wzj.systemc.permission.dto.role;

import lombok.Data;

/**
 * 平台管理员角色获取分页列表请求数据
 */
@Data
public class SaasUserRoleTypePagingRequestVo {
    /**
     * 关键词
     */
    private String keywords;
}
