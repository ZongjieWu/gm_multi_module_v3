package com.wzj.systemc.permission.controller;


import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserPagingRequestVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserUpdateRequestVo;
import com.wzj.systemc.permission.entity.SaasUser;
import com.wzj.systemc.permission.service.ISaasUserService;
import com.wzj.util.AESUtil;
import com.wzj.util.BeanCopyTools;
import com.wzj.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static com.wzj.systemc.permission.service.impl.SaasUserLoginServiceImpl.AES_KEY;

/**
 *  2.平台管理员,添加、修改、删除、查询等接口
 * @author ZongjieWu
 * @since 2020-04-19
 */
@RestController
@RequestMapping("/saasUser")
public class SaasUserController {
    @Autowired
    private ISaasUserService saasUserService;


    /**
     * 根据id获取详情
     * @param token
     * @return
     */
    @GetMapping("/token")
    public Result<SaasUserBaseInfoResponseVo> token(String token){
        return saasUserService.getByToken(token);
    }

    /**
     * 根据id获取详情
     * @param id
     * @return
     */
    @GetMapping("/detail")
    public Result detail(Long id){
        return Result.returnSucessMsgData(saasUserService.getById(id));
    }

    /**
     * 管理员添加
     * @param saasUserAddRequestVo
     * @return
     */
//    @SocketRole(value = "add_saasUser")
    @PostMapping("/add")
    public Result add(@RequestBody @Validated SaasUser saasUserAddRequestVo) {
        SaasUser saasUser= BeanCopyTools.copy(saasUserAddRequestVo,SaasUser.class);
        saasUser.setPwd(MD5Util.getMD5(AESUtil.encode(saasUser.getPwd(), AES_KEY)));
        saasUser.setAddTime(new Date());
        saasUser.setModifyTime(new Date());
        saasUserService.save(saasUser);
        return Result.returnSucess();
    }

//   @ApiOperation(value = "管理员删除",notes = "管理员删除")
//    @ApiImplicitParam(paramType = "query",name = "id",value = "管理员表id",required = true,dataType = "int")
//    @SocketRole(value = "delete_saasUser")
//    @PostMapping("delete")
//    public Result delete(@RequestParam Long id) {
//        saasUserService.removeById(id);
//        return Result.retrunSucess();
//    }

    /**
     * 管理员修改
     * @param saasUserUpdateRequestVo
     * @return
     */
//    @SocketRole(value = "update_saasUser")
    @PostMapping("update")
    public Result update(@RequestBody @Validated SaasUserUpdateRequestVo saasUserUpdateRequestVo) {
        SaasUser saasUser=BeanCopyTools.copy(saasUserUpdateRequestVo,SaasUser.class);
        if(null!=saasUser.getPwd() && !("").equals(saasUser.getPwd())){
            saasUser.setPwd(MD5Util.getMD5(AESUtil.encode(saasUser.getPwd(), AES_KEY)));
        }
        saasUser.setModifyTime(new Date());
        saasUserService.updateById(saasUser);
        return Result.returnSucess();
    }

    /**
     * 分页查询管理员
     * @param page
     * @param limit
     * @param saasUserPagingRequestVo
     * @return
     */
    //    @SocketRole(value = "pageing_saasUser")
    @PostMapping("paging")
    public Result<List<SaasUserBaseInfoResponseVo>> paging(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer limit, SaasUserPagingRequestVo saasUserPagingRequestVo) {
        Result<List<SaasUserBaseInfoResponseVo>> listResult=saasUserService.paging(page,limit,saasUserPagingRequestVo);
        return listResult;
    }
}

