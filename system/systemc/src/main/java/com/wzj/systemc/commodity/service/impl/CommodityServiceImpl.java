package com.wzj.systemc.commodity.service.impl;

import com.wzj.systemc.commodity.entity.Commodity;
import com.wzj.systemc.commodity.dao.CommodityMapper;
import com.wzj.systemc.commodity.service.ICommodityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, Commodity> implements ICommodityService {

}
