package com.wzj.systemc.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dao.SaasUserMapper;
import com.wzj.systemc.permission.dao.SaasUserPermissionListMapper;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserLoginRequestVo;
import com.wzj.systemc.permission.entity.SaasUser;
import com.wzj.systemc.permission.entity.SaasUserPermissionList;
import com.wzj.systemc.permission.service.ISaasUserLoginService;
import com.wzj.util.AESUtil;
import com.wzj.util.BeanCopyTools;
import com.wzj.util.JWTUtils;
import com.wzj.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-19
 */
@Service
public class SaasUserLoginServiceImpl extends ServiceImpl<SaasUserMapper, SaasUser> implements ISaasUserLoginService {

    public static final String AES_KEY = "t89zsda45fdf4wa5";

    @Autowired
    private SaasUserMapper saasUserMapper;
    @Autowired
    private SaasUserPermissionListMapper saasUserPermissionListMapper;
    @Override
    public Result<SaasUserBaseInfoResponseVo> login(SaasUserLoginRequestVo saasUserLoginRequestVo) {
        //查询包装器
        QueryWrapper<SaasUser> queryWrapper = new QueryWrapper<SaasUser>();
        queryWrapper.lambda().eq(SaasUser::getPhone, saasUserLoginRequestVo.getPhone());
        SaasUser userSaasRes=saasUserMapper.selectOne(queryWrapper);
        if(userSaasRes==null){
            return Result.returnFailMsg("此用户不存在");
        }
        if(!MD5Util.getMD5(AESUtil.encode(saasUserLoginRequestVo.getPwd(), AES_KEY)).equals(userSaasRes.getPwd())){
            return Result.returnFailMsg("密码错误");
        }

        //查询菜单
        QueryWrapper<SaasUserPermissionList> queryPermissionWrapper = new QueryWrapper<SaasUserPermissionList>();
        queryPermissionWrapper.lambda().eq(SaasUserPermissionList::getParentId,0);
        queryPermissionWrapper.orderByAsc("sort");
        List<SaasUserPermissionList> saasUserPermissionListList=saasUserPermissionListMapper.selectList(queryPermissionWrapper);
        saasUserPermissionListList.forEach(item->{
            QueryWrapper<SaasUserPermissionList> queryPermission2Wrapper = new QueryWrapper<SaasUserPermissionList>();
            queryPermission2Wrapper = new QueryWrapper<SaasUserPermissionList>();
            queryPermission2Wrapper.lambda().eq(SaasUserPermissionList::getParentId,item.getId());
            queryPermission2Wrapper.orderByAsc("sort");
            item.setSaasUserPermissionList(saasUserPermissionListMapper.selectList(queryPermission2Wrapper));
        });

        SaasUserBaseInfoResponseVo userSaasBaseInfoVo= BeanCopyTools.copy(userSaasRes,SaasUserBaseInfoResponseVo.class);



        //        自定义token
        //        String token = TokenUtil.createToken(new UserTokenDto(userSaasRes.getId(),2,0L));
        Map<String, Object> payload = new HashMap<String, Object>();
        payload.put("loginId", userSaasRes.getId());
        //86400000是过期时间  24小时
        String jwt=null;
        try {
           jwt = JWTUtils.createJWT("jwt", userSaasRes.getPhone(), 86400000,payload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        userSaasBaseInfoVo.setToken(jwt);
        userSaasBaseInfoVo.setSaasUserPermissionList(saasUserPermissionListList);
        return Result.returnSucessMsgData(userSaasBaseInfoVo);
    }
}
