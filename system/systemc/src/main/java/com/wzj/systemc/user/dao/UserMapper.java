package com.wzj.systemc.user.dao;

import com.wzj.systemc.user.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-31
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
    Long getSnowId(Long snowId);

    int insertSonwId(Long snowId);
}
