package com.wzj.systemc.order.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
//@Accessors(chain = true)
@TableName("order")
public class OrderMasterDataDTO implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 订单id
     */
    @TableId("id")
    private Long id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 商品id
     */
    @TableField("commodity_id")
    private Long commodityId;

    /**
     * 商品数量
     */
    @TableField("num")
    private Integer num;

    /**
     * 订单金额
     */
    @TableField("money")
    private BigDecimal money;

    /**
     * 修改时间
     */
    @TableField("modify_datetime")
    private Date modifyDatetime;

    /**
     * 创建时间
     */
    @TableField("create_datetime")
    private Date createDatetime;

    /**
     * #################################
     * #############非标映射字段#######
     * #################################
     */

    /***
     * 用户名称
     */
    private String userName;

    /**
     * 商品名称
     */
    private String commodityName;


}
