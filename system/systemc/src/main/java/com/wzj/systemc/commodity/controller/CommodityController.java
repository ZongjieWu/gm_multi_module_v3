package com.wzj.systemc.commodity.controller;


import com.wzj.config.apiversionconfig.ApiVersion;
import com.wzj.object.result.Result;
import com.wzj.systemc.commodity.entity.Commodity;
import com.wzj.systemc.commodity.service.ICommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *  商品相关
 * @author ZongjieWu
 * @since 2021-03-31
 */
@ApiVersion(3)
@RestController
@RequestMapping("{version}/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;


    /**
     * 根据用id获取信息xxxxx提交到分支3
     * @param id 商品id
     * @return
     */
    @ApiVersion(6)
    @GetMapping(value = "getById")
    public Result<Commodity> getById(Long id){
        System.out.println("我是v6版本api");
        Commodity commodity =commodityService.getById(id);
        return Result.returnSucessMsgData(commodity);
    }

    /**
     * 根据用id获取信息xxxxx提交到分支3
     * @param id 商品id
     * @return
     */
    @ApiVersion(3)
    @GetMapping(value = "getById")
    public Result<Commodity> getById3(Long id){
        System.out.println("我是v3版本api");
        Commodity commodity =commodityService.getById(id);
        return Result.returnSucessMsgData(commodity);
    }
}

