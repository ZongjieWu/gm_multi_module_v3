package com.wzj.systemc.permission.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.systemc.permission.dao.SaasUserRolePermissionMapper;
import com.wzj.systemc.permission.entity.SaasUserRolePermission;
import com.wzj.systemc.permission.service.ISaasUserRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
@Service
public class SaasUserRolePermissionServiceImpl extends ServiceImpl<SaasUserRolePermissionMapper, SaasUserRolePermission> implements ISaasUserRolePermissionService {

    @Autowired
    private SaasUserRolePermissionMapper saasUserRolePermissionMapper;
    @Override
    public List<Long> listByRoleId(Long saasUserRoleId) {
        return saasUserRolePermissionMapper.listByRoleId(saasUserRoleId);
    }
}
