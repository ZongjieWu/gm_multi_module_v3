package com.wzj.systemc.permission.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wzj.systemc.permission.entity.SaasUserRoleType;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-20
 */
@Repository
public interface SaasUserRoleTypeMapper extends BaseMapper<SaasUserRoleType> {

}
