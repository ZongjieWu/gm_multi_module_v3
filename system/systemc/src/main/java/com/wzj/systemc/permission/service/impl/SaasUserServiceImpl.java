package com.wzj.systemc.permission.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wzj.object.result.Result;
import com.wzj.systemc.permission.dao.SaasUserMapper;
import com.wzj.systemc.permission.dto.saasuser.SaasUserBaseInfoResponseVo;
import com.wzj.systemc.permission.dto.saasuser.SaasUserPagingRequestVo;
import com.wzj.systemc.permission.entity.SaasUser;
import com.wzj.systemc.permission.service.ISaasUserService;
import com.wzj.util.BeanCopyTools;
import com.wzj.util.JWTUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ZongjieWu
 * @since 2020-04-19
 */
@Service
public class SaasUserServiceImpl extends ServiceImpl<SaasUserMapper, SaasUser> implements ISaasUserService {
    @Autowired
    private SaasUserMapper saasUserMapper;
//    @Override
//    public Result<List<SaasUserBaseInfoResponseVo>> paging(Integer page, Integer limit, SaasUserPagingRequestVo saasUserPagingRequestVo) {
//        IPage<SaasUser> userPage = new Page<>(page, limit);//参数一是当前页，参数二是每页个数
//        //组装查询器
//        QueryWrapper<SaasUser> saasUserQueryWrapper=new QueryWrapper<SaasUser>();
//        saasUserQueryWrapper.lambda()
//                .like(!StringUtils.isEmpty(saasUserPagingRequestVo.getKeywords()),SaasUser::getPhone,saasUserPagingRequestVo.getKeywords())
//                .or().like(!StringUtils.isEmpty(saasUserPagingRequestVo.getKeywords()),SaasUser::getName,saasUserPagingRequestVo.getKeywords());
//
//        userPage = saasUserMapper.selectPage(userPage, saasUserQueryWrapper);
//        List<SaasUser> saasUserList=userPage.getRecords();
//        List<SaasUserBaseInfoResponseVo> saasUserBaseInfoResponseVoLis=new ArrayList<>();
//        BeanCopyTools.copy(saasUserList,saasUserBaseInfoResponseVoLis,SaasUserBaseInfoResponseVo.class);
//        Result<List<SaasUserBaseInfoResponseVo>> result=Result.retrunSucessMsgData(saasUserBaseInfoResponseVoLis);
//        result.setCount(userPage.getTotal());
//        return result;
//    }

    @Override
    public Result<List<SaasUserBaseInfoResponseVo>> paging(Integer page, Integer limit, SaasUserPagingRequestVo saasUserPagingRequestVo) {
        Page<SaasUser> userPage = new Page<SaasUser>(page, limit);//参数一是当前页，参数二是每页个数
        //组装查询器
        IPage<SaasUser> iPage = saasUserMapper.paging(userPage, saasUserPagingRequestVo);
        List<SaasUser> saasUserList=iPage.getRecords();
        List<SaasUserBaseInfoResponseVo> saasUserBaseInfoResponseVoLis=new ArrayList<>();
        BeanCopyTools.copy(saasUserList,saasUserBaseInfoResponseVoLis,SaasUserBaseInfoResponseVo.class);
        Result<List<SaasUserBaseInfoResponseVo>> result=Result.returnSucessMsgData(saasUserBaseInfoResponseVoLis);
        result.setCount(iPage.getTotal());
        return result;
    }

    @Override
    public Result<SaasUserBaseInfoResponseVo> getByToken(String jwt) {
        SaasUserBaseInfoResponseVo baseInfoResponseVo=null;
        //第一步先解析数据
        Claims c=null;
        try {
            c= JWTUtils.parseJWT(jwt);

            //验证时间
            if(System.currentTimeMillis()<=Long.valueOf(c.get("exp").toString())){
                System.out.println("请求超时哦！");
                return Result.returnFailMsg("登入超时,请重新登入");
            }

            //验证身份
            if(c.get("loginId")==null){
                return Result.returnFailMsg("请联系管理员获取账号1!!!");
            }

            SaasUser saasUser=saasUserMapper.selectById(Long.valueOf(c.get("loginId").toString()));
            //用户不存在
            if(saasUser==null){
                return Result.returnFailMsg("请联系管理员获取账号2!!!");
            }

            //第二部查询数据
            baseInfoResponseVo = BeanCopyTools.copy(saasUser,SaasUserBaseInfoResponseVo.class);
        }catch (ExpiredJwtException e){
            return Result.returnFailMsg("登入超时,请重新登入");
        }catch (Exception e){
            return Result.returnFailMsg("查询错误");
        }
        return Result.returnSucessMsgData(baseInfoResponseVo);
    }
}
