//package com.wzj.config;
//
//import com.alibaba.fastjson.JSONException;
//import com.wzj.object.result.Result;
//import io.jsonwebtoken.MalformedJwtException;
//import io.jsonwebtoken.SignatureException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.validation.BindException;
//import org.springframework.web.HttpRequestMethodNotSupportedException;
//import org.springframework.web.bind.MissingServletRequestParameterException;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
//import org.springframework.web.multipart.MaxUploadSizeExceededException;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//
//
///***
// * 统一异常处理
// * */
//@Slf4j
//@RestControllerAdvice
//public class ExceptionAdvice {
//    /**
//     * 参数类型不一致
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
//    public Result MethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"方法参数类型不匹配"+":->"+e.getMessage());
//    }
//
//    /**
//     * 拦截捕捉自定义异常Exception.class
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = Exception.class)
//    public Result ExceptionHandler(Exception e) {
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        StackTraceElement[] stackTraceElements=e.getStackTrace();
//
//        log.error("#############start#################");
//        log.error("#############start#################");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        for(int i=0;i<stackTraceElements.length;i++){
//            if(stackTraceElements[i].getClassName().contains("Controller")||stackTraceElements[i].getClassName().contains("Impl")){
//                log.error("-----------------------------------------");
//                log.error(stackTraceElements[i].getClass().toString());
//                log.error(stackTraceElements[i].getClassName());
//                log.error(stackTraceElements[i].getMethodName());
//                log.error(stackTraceElements[i].getClassName());
//                log.error(stackTraceElements[i].getFileName());
//                log.error(stackTraceElements[i].getLineNumber()+"");
//                log.error("-----------------------------------------");
//            }
//        }
//        log.error("#############end#################");
//        log.error("#############end#################");
//        e.printStackTrace();
////        return Result.retrunFailMsgData(dataMap);
//        //把异常信息记录到日志里
//        return Result.returnFailMsgData("后台系统错误");
//    }
//
//    /**
//     * 绑定异常，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = BindException.class)
//    public Result BindExceptionHandler(BindException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
////        System.out.println(e.getObjectName());;
////        System.out.println(e.getFieldError().getField());
////        System.out.println(e.getFieldError().getRejectedValue());
////        System.out.println(e.getFieldError().getDefaultMessage());
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"绑定异常"+":"+e.getFieldError().getField()+"->"+e.getFieldError().getDefaultMessage());
//    }
//
//    /**
//     * 签名异常，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = SignatureException.class)
//    public Result SignatureExceptionHandle(SignatureException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"签名错误"+"：token错误");
//    }
//
//    /**
//     * 签名异常，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = MalformedJwtException.class)
//    public Result MalformedJwtExceptionHandle(MalformedJwtException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"签名错误"+"：token错误");
//    }
//
//
//    /**
//     * 请求方法不支持，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
//    public Result HttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"请求方法不支持");
//    }
//    /**
//     * 请求方法不支持，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = MissingServletRequestParameterException.class)
//    public Result MissingServletRequestParameterException(MissingServletRequestParameterException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"请求参数错误"+":"+e.getParameterName()+"->"+e.getParameterType()+"->"+e.getMessage());
//    }
//
//    /**
//     * 请求方法不支持，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = JSONException.class)
//    public Result MissingServletRequestParameterException(JSONException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"请求参数要是Json格式"+":->"+e.getMessage());
//    }
//    /**
//     * 请求方法不支持，用于参数校验
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value = MaxUploadSizeExceededException.class)
//    public Result MaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
//        log.error("#############start#################");
//        log.error("#############start#################");
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.error("错误信息：如下"+sdf.format(new Date()));
//        log.error(e.getClass().getName());
//        log.error(e.getMessage());
//        log.error("#############end#################");
//        log.error("#############end#################");
//        return Result.returnCodeMsg(-1,"文件上传错误"+":->"+e.getMessage());
//    }
//
//
//
//}