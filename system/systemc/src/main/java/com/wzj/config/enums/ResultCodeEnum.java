package com.wzj.config.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回码枚举
 * @author wzj
 * @version 1.0
 * @date 2019/10/16
 */
public enum ResultCodeEnum {

    /**
     * -1和0已经在Result中使用-1失败0成功,需做国际化的时候再用这个文件
     */

    /**
     * #####################################################################################
     * ##############################common公共(系统)的返回码0-99999########################################
     * #####################################################################################
     */
    COMMON_PARAM_ERROR(5,"参数错误"),
    COMMON_TOKEN_ERROR(6,"签名错误"),
    COMMON_REQUEST_METHOD_NOT_SUPPORT(7,"请求方法不支持"),
    COMMON_JSON_FORMAT_ERROR(8,"JSON格式错误"),
    COMMON_PARAM_TYPE_ERROR(9,"参数类型错误"),
    /**
     * #####################################################################################
     * ##############################common公共(业务)的返回码100000-110000########################################
     * #####################################################################################
     */
    COMMON_ACCOUNT_ERROR(100000, "账号错误"),
    COMMON_PWD_ERROR(100001, "密码错误"),
    COMMON_LOGIN_TIMEOUT(100002, "登入超时,请重新登入"),
    COMMON_ACCOUNT_IS_EMPTY(100003, "此账号为空,请用其他账号登入"),
    COMMON_NOT_LOGIN(100004, "请登入"),
    COMMON_PHONE_EXIST(100005, "该手机号已经存在"),
    COMMON_IMG_SIZE_EXCEED(100006, "图片太大了,请控制在5M以内");











    /**
     * #####################################################################################
     * ##############################系统设置模块类的返回码170001-180000########################################
     * #####################################################################################
     */


    /**
     * 值
     */
    private Integer value;
    /**
     * 名
     */
    private String name;

    ResultCodeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * 根据value返回对应的枚举值
     *
     * @param value value
     * @return 对应的枚举值
     */
    public static ResultCodeEnum valueOf(Integer value) {
        switch (value) {
            case 1:
                return COMMON_ACCOUNT_ERROR;
            case 2:
                return COMMON_PWD_ERROR;
            default:
                return COMMON_ACCOUNT_ERROR;
        }
    }

    /**
     * 获取枚举Map封装信息
     *
     * @return map
     */
    public static Map<Integer, String> toMap() {
        Map<Integer, String> map = new HashMap<>();
        ResultCodeEnum[] resultCodeEnums = ResultCodeEnum.values();
        for (ResultCodeEnum agreementType : resultCodeEnums) {
            map.put(agreementType.getValue(), agreementType.getName());
        }
        return map;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}
