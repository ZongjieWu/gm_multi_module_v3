package com.wzj.systemb.dao;

import com.wzj.systemb.entity.Commodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@Repository
public interface CommodityMapper extends BaseMapper<Commodity> {

}
