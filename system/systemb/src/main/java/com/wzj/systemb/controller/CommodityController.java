package com.wzj.systemb.controller;


import com.wzj.object.result.Result;
import com.wzj.systemb.entity.Commodity;
import com.wzj.systemb.service.ICommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品表 前端控制器
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
@RestController
@RequestMapping("/commodity")
public class CommodityController {

    @Autowired
    private ICommodityService commodityService;

    /**
     * 根据id获取商品信息提交到分支2
     * @return
     */
    @GetMapping(value = "getById")
    public Result<Commodity> getById(Long id){
        Commodity commodity=commodityService.getById(id);
        return Result.returnSucessMsgData(commodity);
    }
}

