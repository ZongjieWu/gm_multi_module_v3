package com.wzj.systemb.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wzj.systemb.entity.Commodity;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author ZongjieWu
 * @since 2021-03-23
 */
public interface ICommodityService extends IService<Commodity> {

}
